<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;

class ApiProjectController extends Controller
{
    public function index() {
        return Project::with('tasks')->get()->toJson();
    }

    public function store(Request $request) {
        return Project::create([
            'name' => $request->name,
            'description' => $request->description,
            'status' => $request->status
        ]);
    }

    public function show($id) {
        return Project::with('tasks')->findOrFail($id)->toJson();
    }

    public function destroy($id) 
    {
        $project = Project::findOrFail($id);
        $project->delete();
        
        return response()->json([
            'message' => 'Entity successfully deleted'
        ]);
    }

}
