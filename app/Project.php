<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'status',
    ];

    /**
     * Get all of the tasks for a project.
     */
    public function tasks()
    {
        return $this->hasMany('App\Task');
    }
}
