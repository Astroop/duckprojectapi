<?php

namespace App\Http\Controllers\Api;

use App\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiTaskController extends Controller
{
    public function index() {
        return Task::with('project')->get()->toJson();
    }

    public function store(Request $request)
    {
        return Task::create([
            'name' => $request->name,
            'description' => $request->description,
            'status' => $request->status,
            'project_id' => $request->project_id
        ]);
    }

    
    public function destroy($id) 
    {
        $task = Task::findOrFail($id);
        $task->delete();
        
        return response()->json([
            'message' => 'Entity successfully deleted'
        ]);
    }
}
