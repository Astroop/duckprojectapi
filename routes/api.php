<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'Api\ApiAuthController@login');
Route::post('/register', 'Api\ApiAuthController@register');

Route::group(['middleware' => ['ApiAuth']], function () {
    Route::get('/projects', 'Api\ApiProjectController@index');
    Route::post('/projects', 'Api\ApiProjectController@store');
    Route::get('/projects/{id}', 'Api\ApiProjectController@show');
    Route::delete('/projects/{id}', 'Api\ApiProjectController@destroy');
    
    Route::get('/tasks', 'Api\ApiTaskController@index');
    Route::post('/tasks', 'Api\ApiTaskController@store');
    Route::delete('/tasks', 'Api\ApiTaskController@destroy');
});
