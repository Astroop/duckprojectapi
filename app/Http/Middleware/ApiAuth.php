<?php

namespace App\Http\Middleware;

use Closure;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->api_token) {
            if(\App\User::where('api_token', $request->api_token)->exists()) {
                return $next($request);
            }
        } else {
            return response()->json([
                'message' => 'No token provided'
            ], 401); 
        }

        return response()->json([
            'message' => 'Invalid Token'
        ], 401);
        
    }
}
