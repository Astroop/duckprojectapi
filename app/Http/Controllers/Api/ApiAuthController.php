<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ApiAuthController extends Controller
{
    public function register(Request $request)
    {
        return User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'api_token' => Str::random(60),
        ]);
    }

    public function login(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        
        if($user) {
            if(Hash::check($request->password, $user->password)) {
                return response()->json([
                    'token' => $user->api_token,
                ]);
            }
        }

        return response()->json([
            'message' => 'User not found'
        ], 404);
    }
}
